////  FieldTableViewCell.swift
//  swift_custom_table_view_cells
//
//  Created on 22/04/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class FieldTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    static let identifier = "FieldTableViewCell"
      
    @IBOutlet weak var field: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        field.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // usuwa klawiaturę po akceptacji wpisu
        print("\(textField.text ?? "")")
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static func nib() -> UINib {
          return UINib(nibName: "FieldTableViewCell", bundle: nil)
    }
    
}
